<?php
require_once './templates/header.php';
require './lib/ConnectDB.php';
if ($_SESSION['is_login'] != 1) {
    header("Location:index.php");
}

$connect = ConnectDB::getInstace();
$id_user = addslashes(mysqli_real_escape_string($connect->cDB(),$_GET['user']));
$name = addslashes(mysqli_real_escape_string($connect->cDB(),$_GET['name']));
        $result = $connect->cDB()->query("SELECT * FROM images WHERE id_user ='$id_user' ");
?>
<section>
    <div id="album">
        <h4>Галерия със снимки на <?php echo $name;?></h4>
        <div id="container">
            
        <?php 
        if($result !=null){
        while ($img_r = $result->fetch_assoc()){
        ?>
            <a href="viewimg.php?id=<?php echo $img_r['id_img'];?>" class="example-image-link">
                <img class="example-image"src="<?php echo 'img'.DIRECTORY_SEPARATOR.$name.DIRECTORY_SEPARATOR.$img_r['img_path'];?>" height="150" width="150"></a>
        <?php }}?>
             
     </div>
    </div>
</section>

<?php require_once './templates/footer.php'; ?>