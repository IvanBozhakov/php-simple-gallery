<?php
require_once './templates/header.php';
require './lib/ConnectDB.php';
if ($_SESSION['is_login'] != 1) {
    header("Location:index.php");
}
?>
<script type="text/javascript">
    function deleteImg(id){
        
        if (confirm("Изтриване на Снимката"))
     self.location.href="deleteImg.php?d_id="+id;
    }
</script>
<section>
        
        <div id="wrapperImg">
            
        <?php $connect = ConnectDB::getInstace();
        
        $img_id = addslashes(mysqli_real_escape_string($connect->cDB(),$_GET['id']));
         $img = $connect->cDB()->query('SELECT * FROM images,users where images.id_img='.$img_id.' && users.id_user=images.id_user');
         
        $result_comments = $connect->cDB()->query('SELECT images.id_img, images.img_path, images.description, comments.date, users.username, comments.comments
FROM images
LEFT JOIN comments ON images.id_img = comments.id_img
LEFT JOIN users ON comments.id_user = users.id_user
WHERE images.id_img =  '.$img_id.' && comments.id_img='.$img_id.' ORDER BY date DESC');
        
        $img_result = $img->fetch_assoc();
       
        ?>
              <div class="image-row">
			<a class="example-image-link" href="<?php echo 'img'.DIRECTORY_SEPARATOR.$img_result['username'].DIRECTORY_SEPARATOR.$img_result['img_path'];?>" title="<?php echo $img_result['description'];?>" data-lightbox="example-1"><img class="example-image" src="<?php echo 'img'.DIRECTORY_SEPARATOR.$img_result['username'].DIRECTORY_SEPARATOR.$img_result['img_path'];?>" alt="" width="400" height="400"/></a>
		          <?php if($_SESSION['rank']==2 ||$_SESSION['rank']==1 || $_SESSION['id_user']==$img_result['id_user']){?>
                        <br/><a id="deletebtn" title="Изтриване на снимката" href="javascript:deleteImg(<?php echo $img_result['id_img']; ?>)" ><img src="img/close.png" alt=""/></a>
                          <?php }?>
              </div>
               
               <div id='descriptWrapper'>
                   <b>Описание:</b>
                   <i><?php echo $img_result['description'];?><i>
                   <div>
               <form method="post" action="insertComment.php">
                   <b>Добави коментар</b><br/>
                   <textarea rows="4" cols="50" name="comment"></textarea><br>
                   <input type="hidden" name="c_id_img" value="<?php echo $img_id;?>">
                   <input type="hidden" name="c_id_user" value="<?php echo $_SESSION['id_user'];?>">
                   <input type="submit" class="buttons" name="submit" value='Добави'/>
               </form>
                      
                       <?php if($result_comments->num_rows != 0){?>
                       <h5>Коментари:</h5>
                       <div id='comments'>
                           
                           <?php
                           
                           while($row = $result_comments->fetch_assoc()){
                               echo '<span id="date_format">'.date("m/d/y H:i",$row['date']).'</span> ';
                               echo '<b>'.$row['username'].'</b>:'."  ";
                               echo '<i>'.$row['comments'].'</i><br/>';
                           }?>
                       </div>
               <?php }?>
          
     </div>
    
</section>
<?php require_once './templates/footer.php'; ?>
