<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ConnectDB
 *
 * @author Ivan Bozhakov
 */
class ConnectDB {
    private $_server = 'localhost';
    private $_username = 'root';
    private $_password = '';
    protected $_database = 'tbl_gallery';
    private static $_instance;

   
    private function __construct() {
      
    }
    
    public static function getInstace() {
        if (!isset(self::$_instance)) {
            $c = __CLASS__;
            self::$_instance = new $c;
        }

        return self::$_instance;
    }

    public function cDB() {
        $connect = new mysqli($this->_server, $this->_username, $this->_password,$this->_database);
         $connect->query('SET NAMES UTF8');
        if (!$connect) {
            die('Could not connect: ' . $connect->connect_error);
        }
       return $connect;
    }
}

?>
