<?php
require_once './templates/header.php';
require './lib/ConnectDB.php';
if ($_SESSION['is_login'] != 1) {
    header("Location:index.php");
}
$connect = ConnectDB::getInstace();
$resul_users = $connect->cDB()->query('SELECT * FROM users');
?>

<section>
    <div id="wrapperImg">
        <table border="1">
            <tr>
                <th>Име</th>
                    <th>Права</th>
                        <th>Емайл</th>
                            <th>Снимки на потребителя</th>
                                 <th>Направи Администратор</th>
            </tr>
            <?php while ($user = $resul_users->fetch_assoc()) {
                ?>
                <tr>
                    <td><?php echo $user['username'];?></td>
                        <td><?php if($user['rank']==1 || $user['rank']==2 )echo '<span id="admin_field">'."Администратор".'</span>';else echo "Няма";?></td>
                            <td><?php echo $user['email'];?></td>
                                <td><?php echo '<a href="view.php?user='.$user['id_user'].'&name='.$user['username'].'">'.'Виж'.'</a>';?></td>
                                <td><?php if($_SESSION['rank']!=0){
                                    if($user['rank']==1 )
                                    echo '<a href="setadmin.php?user='.$user['id_user'].'&r=0">'.'Премахни'.'</a>';
                                elseif($user['rank']==0)
                                    echo '<a href="setadmin.php?user='.$user['id_user'].'&r=1">'.'Сложи'.'</a>';
                                }?></td>
                </tr>
            <?php } ?>
        </table> 

    </div>

</section>
<?php require_once './templates/footer.php'; ?>
