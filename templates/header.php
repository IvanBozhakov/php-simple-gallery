<?php date_default_timezone_set("Europe/Sofia");
session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Яката Галерия :D</title>
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script src="js/jquery-1.10.2.min.js"></script>
        <script src="js/lightbox-2.6.min.js"></script>
        <link href="css/lightbox.css" rel="stylesheet" />
        <link rel="stylesheet" href="css/screen.css" media="screen"/>

    </head>

    <body>
        <header>
           
            <h2>Галерия</h2>
            <span class="sayHello"><b>Здравей</b>, <?php if (!empty($_SESSION['name'])) echo $_SESSION['name']; ?></span>
            <nav>
                <a class="exit" href="destroy.php">Изход</a>
                <a class="users" href="users.php">Потребители</a>
                <a class="dashboard" href="dashboard.php">Моите снимки</a>
            </nav>
        </header>